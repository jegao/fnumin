
/////////////////////////////////////////////////////////////////////////
// G4Tutorial:
//
// UserTrackingAction.hh
// 
// Defining actions performed at the start/end point of processing a
// track
// https://www.ge.infn.it/geant4/training/ornl_2008/day4/code/UserTrackingAction.cc
//
/////////////////////////////////////////////////////////////////////////


#include "UserTrackingAction.hh"
#include "FaserTrackInformation.hh"  // To add parent particle to G4Track
#include "G4TrackingManager.hh"
#include "G4Electron.hh"


// Define constructor
UserTrackingAction::UserTrackingAction() :
    counter(0) {

}


void TrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
  FaserTrackInformation* info = dynamic_cast<FaserTrackInformation*>(aTrack->GetUserInformation());
	if (info->GetTrackingStatus() > 0)
	{
		fpTrackingManager->SetStoreTrajectory(true);
	}
	else 
	{
		fpTrackingManager->SetStoreTrajectory(false);
	}



void TrackingAction::PostUserTrackingAction(const G4Track* aTrack) {

  // The tracking action class holds the pointer to the tracking manager:
  // fpTrackingManager

  // From the tracking manager we can retrieve the secondary track vector,
  // which is a container class for tracks:
  G4TrackVector* secTracks = fpTrackingManager -> GimmeSecondaries();

  // You can use the secTracks vector to retrieve the number of secondary 
  // electrons
  if(secTracks) { 
     FaserTrackInformation* info = (FaserTrackInformation*) (aTrack->GetUserInformation());

     size_t nmbSecTracks = (*secTracks).size();       

     for(size_t i = 0; i < nmbSecTracks; i++) { 
        if((*secTracks)[i] -> GetDefinition() == G4Electron::Definition()) {
              counter++;
        }
        FaserTrackInformation* infoNew;
        if (aTrack->GetParentID() == 0) {
            infoNew = new FaserTrackInformation((*secTracks)[i]);
        } else {
            infoNew = new FaserTrackInformation(info);
        }
        (*secTracks)[i]->SetUserInformation(infoNew);
     }
  }
}
